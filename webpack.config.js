const path = require('path');

module.exports = {
    mode: 'development',
    devtool: 'eval-source-map', // for better readability of code on the devtool(browser's)
    entry: './src/index.ts',
    module: {
        rules: [
            {
                test: /\.ts$/, // if the test passes
                use: 'ts-loader', // it will use this the package we installed to compile
                include: [path.resolve(__dirname, 'src')] // inside the `src`
            }
        ]
    },
    resolve: {
        extensions: ['.ts','.js'] // So webpack know what type are we goingt to import
    },
    output: {
        publicPath: 'public',
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'public')
    }
}